import { Hexagon } from 'constants/hexagonShape'
import {
  COLORS,
  BORDER_RADIUS,
  FONT_SIZE,
  LINE_HEIGHT,
  LANDSCAPE,
} from 'constants/theme'

import React, { useCallback, useState } from 'react'
import styled, { css } from 'styled-components'
import { Layout } from 'components/Layout'
import { tablet, mobile, laptop } from 'utils/CssUtils'
import { Heading } from 'components/Heading'
import { Text } from 'components/Text'
import { Button } from 'components/Button'
import { NextPage } from 'next'
import { Dropzone } from 'components/Dropzone'
import { FormResponseApi } from 'services/FormResponseApi'
import { Client } from 'filestack-js'
import { Spinner } from 'components/Icons/Spinner'
import { Check } from 'components/Icons/Check'
import { motion } from 'framer-motion'

const Container = styled.div`
  margin-left: 120px;
  height: 100%;
  position: relative;

  ${tablet`
    margin-left: 80px;
    overflow-x: hidden;
    overflow-y: scroll;
  `}

  ${LANDSCAPE.MOBILE} {
    margin-left: 0;
  }

  ${mobile`
    margin-left: 0;
    overflow-y: scroll;
  `};
`

const HeadingSection = styled(motion.div)`
  filter: drop-shadow(0px 0px 20px #000);
  height: 100%;

  ${mobile`
    height: 40%;
  `};
`

const HexagonTopSection = styled.div`
  width: 65%;
  padding-top: 70%;
  background: linear-gradient(180deg, #3937a9 0%, #4f8cd2 100%);
  transform: rotate(-7deg) translate(-1%, -50%);
  clip-path: ${Hexagon};
  position: relative;

  ${tablet`
    width: 120%;
    padding-top: 120%;
    transform: rotate(-7deg) translate(-18%, -50%);
  `}

  ${mobile`
    width: 160%;
    padding-top: 160%;
  `}
`

const TextContainer = styled.div`
  position: absolute;
  left: 50%;
  top: 53%;
  width: 70%;
  color: #fff;
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  transform: rotate(7deg) translate(-50%, 30%);

  ${tablet`
    left: 55%;
    top: 48%;
    width: 50%;
  `}

  ${LANDSCAPE.MOBILE} {
    left: 60%;
  }

  ${mobile`
    top: 62%;
  `}
`

const StyledHeading = styled(Heading)`
  ${tablet`
    font-size: ${FONT_SIZE.HEADING};
    line-height: ${LINE_HEIGHT.HEADING};
  `}

  ${mobile`
    font-size: ${FONT_SIZE.LARGE};
    line-height: ${LINE_HEIGHT.LARGE};
  `}
`

const SubHeading = styled(Text)`
  ${mobile`
    display: none;
  `}
`

const FormSection = styled(motion.div)`
  position: absolute;
  right: 5%;
  bottom: 5%;
  width: 40%;

  ${laptop`
    bottom: 10%;
  `}

  ${tablet`
    width: 100%;
    left: 0;
  `}


  ${LANDSCAPE.MOBILE} {
    position: relative;
    margin-top: -15%;
  }

  ${mobile`
    position: relative;
    padding-bottom: 40px;
  `}
`

const FormContainer = styled.form`
  margin: 0 auto;
  color: #fff;
  display: flex;
  justify-content: center;
  flex-direction: column;

  ${tablet`
    width: 80%;
    height: 100%;
  `}
`

const SuccessContainer = styled.div`
  display: flex;
  flex-direction: column;
  text-align: center;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;
`

const SuccessMark = styled.div`
  width: 150px;
  height: 150px;
  padding: 40px;
  background-color: ${COLORS.GREEN};
  clip-path: ${Hexagon};
`

const SuccessText = styled.div`
  color: ${COLORS.BLACK};
  font-size: ${FONT_SIZE.MEDIUM};
  width: 100%;
  margin-top: 20px;
`

const inputSharedCss = css`
  width: 100%;
  border-radius: ${BORDER_RADIUS.ROUND};
  padding: 20px;
  margin-top: 20px;
  font-size: ${FONT_SIZE.MEDIUM};
  text-align: right;
  height: 70px;
  background: ${COLORS.LIGHT_GREY};

  ${laptop`
    height: 50px;
    font-size: ${FONT_SIZE.BASE};
  `};
`

const StyledInput = styled.input`
  ${inputSharedCss}
`

const StyledTextInput = styled.textarea`
  ${inputSharedCss}
  height: 200px;
  resize: none;

  ${laptop`
     height: 120px;
  `}
`

const FooterSection = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  ${tablet`
    flex-direction: column;
  `}
`

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: black;
  min-width: 150px;
  height: 40px;
  margin-left: 20px;
  margin-top: 20px;
  border: 2px dashed ${COLORS.BLACK};
  background: ${COLORS.WHITE};
  border-radius: ${BORDER_RADIUS.MAIN};
`

const StyledButton = styled(Button)<{ isResponseSent: boolean }>`
  min-width: 150px;
  width: 30%;
  height: 70px;
  margin-left: 20px;
  margin-top: 20px;
  align-self: flex-end;
  padding: 0;
  border: none;
  border-radius: ${BORDER_RADIUS.ROUND};
  font-size: ${FONT_SIZE.MEDIUM};
  background-color: ${({ isResponseSent }) => isResponseSent && COLORS.GREEN};

  ${laptop`
    height: 50px;
    font-size: ${FONT_SIZE.BASE};
  `}
`

const Contact: NextPage = () => {
  const [fileToUpload, setFileToUpload] = useState<any>({})
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isResponseSent, setIsResponseSent] = useState(false)

  const onDrop = useCallback((acceptedFiles) => {
    if (acceptedFiles?.length > 0) {
      setFileToUpload(acceptedFiles[0])
    }
  }, [])

  const onSubmit = async (event: any) => {
    event.preventDefault()
    setIsSubmitting(true)

    const name = event.target.name?.value
    const company = event.target.company?.value
    const message = event.target.message?.value

    let file = ''

    try {
      if (fileToUpload?.path != null) {
        const clientResponse = await new Client(
          process.env.FILESTACK_API_KEY
        ).upload(fileToUpload)

        file = clientResponse?.url
      }
    } catch (error) {
      file = error?.toString()
    }

    FormResponseApi.sendFormResponse({
      name,
      company,
      message,
      file,
    })

    setIsResponseSent(true)
    setIsSubmitting(false)
  }

  return (
    <Layout metaTitle="Lets get in touch">
      <Container>
        <HeadingSection
          initial={{ x: -1000, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          exit={{ x: -1000, opacity: 0 }}
          transition={{ duration: 0.5 }}
          key="heading"
        >
          <HexagonTopSection>
            <TextContainer>
              <StyledHeading margin="0 0 24px 0">
                Just one more step
              </StyledHeading>
              <SubHeading margin="0 0 24px 0">
                No matter if you have an offer, question or just want to say hi.
                Do not hesitate to contact us.
              </SubHeading>
            </TextContainer>
          </HexagonTopSection>
        </HeadingSection>

        <FormSection
          initial={{ x: 1000, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          exit={{ x: 1000, opacity: 0 }}
        >
          <FormContainer onSubmit={onSubmit}>
            {isResponseSent ? (
              <SuccessContainer>
                <SuccessMark>
                  <Check />
                </SuccessMark>
                <SuccessText>Successfully sent! Thank you!</SuccessText>
              </SuccessContainer>
            ) : (
              <>
                <StyledInput placeholder="Name" name="name" required />
                <StyledInput placeholder="Company name" name="company" />
                <StyledTextInput
                  placeholder="Purpose / Message"
                  name="message"
                  required
                />
                <FooterSection>
                  <Dropzone onDrop={onDrop} />
                  {isSubmitting ? (
                    <SpinnerContainer>
                      <Spinner />
                    </SpinnerContainer>
                  ) : (
                    <StyledButton type="submit" isResponseSent={isResponseSent}>
                      Send
                    </StyledButton>
                  )}
                </FooterSection>
              </>
            )}
          </FormContainer>
        </FormSection>
      </Container>
    </Layout>
  )
}

export default Contact
