import React, { ReactNode } from 'react'
import NextLink from 'next/link'

interface LinkProps {
  children: ReactNode
  href: string
  onClick?: () => void
}

export const Link: React.FC<LinkProps> = ({ children, href, ...props }) => (
  <NextLink href={href}>
    <a {...props}>{children}</a>
  </NextLink>
)
