import React from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'

const TransitionContainer = styled(motion.div)`
  width: 100%;
  height: 100%;
`

const variants = {
  initial: { y: 100, opacity: 0 },
  exit: {
    y: 100,
    opacity: 0,
    transition: {
      type: 'tween',
      duration: 0.3,
      ease: 'easeIn',
    },
  },
  animate: {
    y: 0,
    opacity: 1,
    transition: {
      type: 'tween',
      duration: 0.3,
      ease: 'easeIn',
      delay: 0.4,
    },
  },
}

export const PageTransition = ({ children }) => {
  return (
    <TransitionContainer
      initial="initial"
      animate="animate"
      exit="exit"
      variants={variants}
    >
      {children}
    </TransitionContainer>
  )
}
